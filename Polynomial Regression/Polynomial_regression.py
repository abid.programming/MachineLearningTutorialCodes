import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
#from sklearn.cross_validation import train_test_split
from sklearn.linear_model import LinearRegression as lr
from sklearn.preprocessing import PolynomialFeatures as pf

dataset = pd.read_csv('Position_Salaries.csv')

X = dataset.iloc[:, 1:2].values
y = dataset.iloc[:,-1].values


#X_train,X_test,y_train,y_test = train_test_split(X,y,test_size=.0,random_state=0)
print (X)

lreg = lr()
lreg.fit(X, y)

preg = pf(degree = 3)
X_poly = preg.fit_transform(X)
print(X_poly)

#fitting polynomial regressors
lreg2 = lr()
lreg2.fit(X_poly,y)

#Plotting the results
'''plt.scatter(X , y , color= 'red')
plt.plot(X, lreg.predict(X),color = 'blue')
plt.title("LinearRegression Result")
plt.xlabel('Position Level')
plt.ylabel('Salary')
plt.show()'''

plt.scatter(X , y , color= 'blue')
plt.plot(X, lreg2.predict(preg.fit_transform(X)),color = 'red')
plt.title("PolynomialLinearRegression Result")
plt.xlabel('Position Level')
plt.ylabel('Salary')
plt.show()
