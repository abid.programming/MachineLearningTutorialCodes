import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.preprocessing import Imputer
from sklearn.preprocessing import LabelEncoder , OneHotEncoder
from sklearn.cross_validation import train_test_split
from sklearn.preprocessing import StandardScaler


dataset = pd.read_csv('/home/abid/Tutorials-and-Projects/Machine Learning/Machine Learning A-Z/Machine Learning A-Z Template Folder/Part 1 - Data Preprocessing/Section 2 -------------------- Part 1 - Data Preprocessing --------------------/Data.csv')

X = dataset.iloc[:,:-1].values
y = dataset.iloc[:,-1].values

imputer = Imputer(missing_values = 'NaN',strategy = 'mean',axis = 0)
imputer = imputer.fit(X[:,1:3])
X[:,1:3] = imputer.transform(X[:,1:3] )

LabelEncoder_X = LabelEncoder()
X[:,0] = LabelEncoder_X.fit_transform(X[:,0])

OneHotEncoder_X = OneHotEncoder(categorical_features = [0])
X = OneHotEncoder_X.fit_transform(X).toarray()

LabelEncoder_y = LabelEncoder()
y = LabelEncoder_y.fit_transform(y)


X_train,X_test,y_train,y_test = train_test_split(X,y,test_size=0.2,random_state = 0)

SC_X = StandardScaler()
X_train = SC_X.fit_transform(X_train)
X_test = SC_X.transform(X_test)
