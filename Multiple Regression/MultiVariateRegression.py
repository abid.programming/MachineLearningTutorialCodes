from sklearn.linear_model import LinearRegression
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.preprocessing import Imputer
from sklearn.preprocessing import LabelEncoder , OneHotEncoder
from sklearn.cross_validation import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression as Lr
import statsmodels.formula.api as sm

dataset = pd.read_csv('50_Startups.csv')

X = dataset.iloc[:,:-1].values
y = dataset.iloc[:,-1].values

labelencoder_x = LabelEncoder()
X[:,3] = labelencoder_x.fit_transform(X[:,3])
onehotencoder = OneHotEncoder(categorical_features = [3])
X = onehotencoder.fit_transform(X).toarray()

X = X[:,1:]

X_train,X_test,y_train,y_test = train_test_split(X,y,test_size=0.2,random_state=0)


lr = Lr()
lr.fit(X_train,y_train)

y_pred = lr.predict(X_test)


#Using backward Elimination
X = np.append(arr=np.ones((50,1)).astype(int),values=X,axis = 1)
X_opt = X[: , [0,1,2,3,4,5]]
lr_OLS = sm.OLS(endog = y,exog=X_opt).fit()
print(lr_OLS.summary())

X_opt = X[: , [0,1,3,4,5]]
lr_OLS = sm.OLS(endog = y,exog=X_opt).fit()
print(lr_OLS.summary())

X_opt = X[: , [0,3,4,5]]
lr_OLS = sm.OLS(endog = y,exog=X_opt).fit()
print(lr_OLS.summary())

X_opt = X[: , [0,3,5]]
lr_OLS = sm.OLS(endog = y,exog=X_opt).fit()
print(lr_OLS.summary())

X_opt = X[: , [0,5]]
lr_OLS = sm.OLS(endog = y,exog=X_opt).fit()
print(lr_OLS.summary())
