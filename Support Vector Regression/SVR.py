import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
#from sklearn.cross_validation import train_test_split
from sklearn.svm import SVR
from sklearn.preprocessing import StandardScaler
dataset = pd.read_csv('Position_Salaries.csv')

X = dataset.iloc[:, 1:2].values
y = dataset.iloc[:,2:].values


#X_train,X_test,y_train,y_test = train_test_split(X,y,test_size=.0,random_state=0)

#Fitting SVM to the dataset
sc_X = StandardScaler()
sc_y = StandardScaler()
X = sc_X.fit_transform(X)
y = sc_y.fit_transform(y)

svr = SVR(kernel = 'rbf')
svr.fit(X, y)

y_pred = svr.predict(sc_X.transform(np.array([[6.5]])))
print(sc_y.inverse_transform(y_pred))

#Plotting the results
plt.scatter(X , y , color= 'red')
plt.plot(X, svr.predict(X),color = 'blue')
plt.title("SuportVectorRegression  Result")
plt.xlabel('Position Level')
plt.ylabel('Salary')
plt.show()
